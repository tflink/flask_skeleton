# copyright: (c) 2012 by Josef Skladanka <jskladan@redhat.com>, some content borrowed from Tim Flink <tflink@redhat.com>
# license: BSD, see LICENSE for more details.

class Config(object):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'sqlite://'


class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    TRAP_BAD_REQUEST_ERRORS=True
    SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/skeleton_db.sqlite'


class TestingConfig(Config):
    TESTING = True
