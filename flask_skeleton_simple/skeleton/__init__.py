# copyright: (c) 2012 by Josef Skladanka <jskladan@redhat.com>, some content borrowed from Tim Flink <tflink@redhat.com>
# license: BSD, see LICENSE for more details.

from flask import Flask, render_template
from flask.ext.login import LoginManager
from flask.ext.sqlalchemy import SQLAlchemy

import logging
import os


logging.basicConfig(level=logging.INFO,
                    format='[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s',
                    datefmt='%Y%m%d-%H:%M%p')

# Flask App
app = Flask(__name__)
app.secret_key = 'not-really-a-secret'

# load default configuration
if os.getenv('DEV') == 'true':
    app.config.from_object('skeleton.config.DevelopmentConfig')
elif os.getenv('TEST') == 'true':
    app.config.from_object('skeleton.config.TestingConfig')
else:
    if app.secret_key == 'not-really-a-secret':
        raise Warning("You need to change the app.secret_key value for production")
    app.config.from_object('skeleton.config.ProductionConfig')


# setup logging
if app.debug:
    logging.basicConfig(level=logging.DEBUG,
                       format='[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s',
                       datefmt='%Y%m%d-%H:%M%p')


app.logger.debug('using DBURI: %s' % app.config['SQLALCHEMY_DATABASE_URI'])

# database 
db = SQLAlchemy(app)

# setup login manager
login_manager = LoginManager()
login_manager.setup_app(app)
login_manager.login_view = 'login_page.login'

# register blueprints
from skeleton.controllers.main import main
app.register_blueprint(main)

from skeleton.controllers.login_page import login_page
app.register_blueprint(login_page)

from skeleton.controllers.admin import admin
app.register_blueprint(admin)
