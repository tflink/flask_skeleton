# copyright: (c) 2012 by Josef Skladanka <jskladan@redhat.com>, some content borrowed from Tim Flink <tflink@redhat.com>
# license: BSD, see LICENSE for more details.

from flask import Blueprint, render_template
from skeleton import app

main = Blueprint('main', __name__)

@main.route('/')
@main.route('/index')
def index():
    if app.debug:
        app.logger.debug('rendering index')


    return render_template('index.html', title = 'skeleton Main Page')
