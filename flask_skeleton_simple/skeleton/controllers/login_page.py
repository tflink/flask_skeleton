# copyright: (c) 2012 by Josef Skladanka <jskladan@redhat.com>, some content borrowed from Tim Flink <tflink@redhat.com>
# license: BSD, see LICENSE for more details.


from flask import Blueprint, render_template, redirect, flash, url_for, request
from flask.ext.wtf import Form, TextField, PasswordField, HiddenField
from flask.ext.login import login_user, logout_user, login_required, current_user, AnonymousUser


from skeleton import app, login_manager
from skeleton.models.user import User

login_page = Blueprint('login_page', __name__)


class LoginForm(Form):
    username = TextField(u'Username')
    password = PasswordField(u'Password')
    next_page = HiddenField()


# handle login stuff
@login_manager.user_loader
def load_user(userid):
    app.logger.debug("getting info for user %s" % str(userid))
    user = User.query.get(userid)
    if user:
        return user
    else:
        return AnonymousUser


@login_page.route('/login', methods=['GET', 'POST'])
def login():
    login_form = LoginForm()

    if login_form.validate_on_submit():
        user = User.query.filter_by(username = login_form.username.data).first()
        if user and user.check_password(login_form.password.data):
            login_user(user)

            app.logger.info('Successful login for user %s' % login_form.username.data)
            flash('Logged In Successfully!')

            return redirect(login_form.next_page.data)
        else:
            app.logger.info('FAILED login for user %s' % login_form.username.data)
            flash('Login Failed! Please Try again!')

    login_form.next_page.data = request.args.get('next') or url_for('main.index')
    return render_template('login.html', form = login_form)


@login_page.route('/logout')
@login_required
def logout():
    app.logger.info('logout for user %s' % current_user.username)
    logout_user()
    flash('Logged Out Successfully!')
    return redirect(url_for('main.index'))

