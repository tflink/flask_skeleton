# copyright: (c) 2012 by Josef Skladanka <jskladan@redhat.com>, some content borrowed from Tim Flink <tflink@redhat.com>
# license: BSD, see LICENSE for more details.

from flask import Blueprint, render_template, flash, url_for
from flask.ext.login import login_required


admin = Blueprint('admin', __name__)

@admin.route('/admin')
@admin.route('/admin/')
@login_required
def admin_index():
    return render_template('admin/index.html')

