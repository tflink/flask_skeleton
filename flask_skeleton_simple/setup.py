# copyright: (c) 2012 by Josef Skladanka <jskladan@redhat.com>, some content borrowed from Tim Flink <tflink@redhat.com>
# license: BSD, see LICENSE for more details.

from setuptools import setup

setup(name='skeleton',
      version='0.0.1',
      description='',
      author='',
      author_email='',
      license='GPLv2+',
      packages=['skeleton', 'skeleton.controllers', 'skeleton.models'],
      package_dir={'skeleton':'skeleton'},
      include_package_data=True,
      install_requires = [
        'Flask==0.9',
        'Flask-SQLAlchemy==0.16',
        'Flask-WTF>=0.8',
        'Flask-Login>=0.1.3',
     ]
     )
