# copyright: (c) 2012 by Josef Skladanka <jskladan@redhat.com>, some content borrowed from Tim Flink <tflink@redhat.com>
# license: BSD, see LICENSE for more details.

from optparse import OptionParser
import sys


def initialize_db():
    from skeleton import db
    print "Initializing Database"
    db.drop_all()
    db.create_all()

def mock_data():
    from skeleton import db
    from skeleton.models.user import User
    data = [('admin', 'admin'), ('user', 'user')]
    
    for d in data:
        u = User(*d)
        db.session.add(u)
    db.session.commit()
    


if __name__ == '__main__':

    possible_commands = ['init_db', 'mock_data']

    usage = 'usage: [DEV=true] %prog ' + "(%s)" % ' | '.join(possible_commands)
    parser = OptionParser(usage=usage)

    (options, args) = parser.parse_args()

    if len(args) < 1:
        print usage
        print
        print 'Please use one of the following commands: %s' % str(possible_commands)
        sys.exit(1)


    command = args[0]
    if not command in possible_commands:
        print 'Invalid command: %s' % command
        print 'Please use one of the following commands: %s' % str(possible_commands)
        sys.exit(1)


    if command == 'init_db':
        initialize_db()
    elif command == 'mock_data':
        mock_data()
    
    sys.exit(0)
