# copyright: (c) 2012 by Josef Skladanka <jskladan@redhat.com>, some content borrowed from Tim Flink <tflink@redhat.com>
# license: BSD, see LICENSE for more details.

import skeleton

if __name__ == '__main__':
    skeleton.app.run(host = '0.0.0.0', debug = True)
