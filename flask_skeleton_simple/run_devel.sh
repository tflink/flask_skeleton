#!/usr/bin/env bash

# copyright: (c) 2012 by Josef Skladanka <jskladan@redhat.com>, some content borrowed from Tim Flink <tflink@redhat.com>
# license: BSD, see LICENSE for more details.

DEV=true python $1
