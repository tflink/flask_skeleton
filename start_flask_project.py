#!/usr/bin/env python

# Copyright 2009, Red Hat, Inc.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
#
# Author: Josef Skladanka <jskladan@redhat.com>


import os
import sys
import re

if __name__ == "__main__":

   t = raw_input("Select Skeleton type (simple, [bootstrap]):").lower()
   if t not in ['simple', 'bootstrap']:
       t = 'bootstrap'

   dirname = "./flask_skeleton_%s" % t

   if not os.path.isdir(dirname):
       print "Skeleton project not found"
       sys.exit(1)
   re_clean = re.compile(r"[^a-zA-Z0-9_]")
   projname = raw_input("New project name: ")
   projname = re_clean.sub("_", projname.strip())

   if len(projname) == 0:
       print "Project name must be longer than 0 chars"
       sys.exit(2)

   if os.path.exists("./%s" % projname):
       print "File or directory called '%s' already exists" % projname
       sys.exit(3)

   print "Creating new project: %s" % projname
   os.system(r"cp -aL %s ./%s" % (dirname, projname))
   os.system(r"mv ./%s/skeleton ./%s/%s" % (projname, projname, projname))
   os.system(r"grep -Rl skeleton ./%s | xargs -I{} sed -i -e 's/skeleton/%s/g' {}" % (projname, projname))
